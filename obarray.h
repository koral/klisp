#ifndef _data_h
#define _data_h

#include "lisp.h"

int obarray_init(void);
lisp_obj_t obarray_add_entry(char *sym_name);
void obarray_destroy(void);

#endif
