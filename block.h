#ifndef _BLOCK_H_
#define _BLOCK_H_

#include <libgccjit.h>
#include <stddef.h>

#include "korallisp.h"
#include "hash.h"

/*
   Header only basic blocks wrapper.
   Rationale: we need to retrive basic blocks by name and to check if they were
   already closed.
*/

typedef hash_tab_t tag_block_env_t;

typedef struct tag_block_s {
	char *name; /* Must be first */
	gcc_jit_block *gcc_block;
	bool terminated;
} tag_block_t;
COMPILE_ASSERT(!offsetof(tag_block_t, name));

static inline unsigned hash_block(void *ptr)
{
	unsigned res;
	char *name = ((tag_block_t *)ptr)->name;

	while (*name) {
		res = *name;
		name++;
	}

	return res;
}

static inline int cmp_block(void *a, void *b)
{
	char *xa, *xb;

	xa = a;
	xb = b;

	return strcmp(xa, xb);
}

static inline tag_block_env_t tag_block_env_init(void)
{
	return hash_t_create(hash_block, cmp_block, NULL);
}

static inline void tag_block_env_destroy(tag_block_t *tag_blk_env)
{
	hash_t_destroy(tag_blk_env);
}

static inline tag_block_t *tag_block_create(
	hash_tab_t h, gcc_jit_function *func, char *name)
{
	DALLOC(tag_block_t, *tb);
	tb->terminated = FALSE;
	tb->name = name;
	tb->gcc_block = gcc_jit_function_new_block(func, name);
	return h_add(h, tb);
};

static inline tag_block_t *tag_block_search(hash_tab_t h, char *name)
{
	return h_search(h, &name);
}

static inline void tag_block_end_with_conditional (tag_block_t *block,
						gcc_jit_location *loc,
						gcc_jit_rvalue *boolval,
						tag_block_t *on_true,
						tag_block_t *on_false)
{
	if (block->terminated)
		assert(0);

	gcc_jit_block_end_with_conditional(
		block->gcc_block,
		loc,
		boolval,
		on_true->gcc_block,
		on_false->gcc_block);
	block->terminated = TRUE;
}

static inline void tag_block_end_with_jump(tag_block_t *block,
					gcc_jit_location *loc,
					tag_block_t *target)
{
	if (block->terminated)
		assert(0);

	gcc_jit_block_end_with_jump(block->gcc_block,
				loc,
				target->gcc_block);
	block->terminated = TRUE;
}

static inline void tag_block_end_with_return(tag_block_t *block,
					gcc_jit_location *loc,
					gcc_jit_rvalue *rvalue)
{
	if (block->terminated)
		assert(0);

	gcc_jit_block_end_with_return(block->gcc_block,
				loc,
				rvalue);
	block->terminated = TRUE;
}

static inline void tag_block_end_with_void_return(tag_block_t *block,
						gcc_jit_location *loc)
{
	if (block->terminated)
		assert(0);

	gcc_jit_block_end_with_void_return(block->gcc_block, loc);
	block->terminated = TRUE;
}

#endif
