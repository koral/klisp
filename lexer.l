
%{
#include <string.h>
#include <stdarg.h>
#include "lisp.h"
#include "parser.h"

#define YY_NO_INPUT

unsigned line = 1;

%}

%option outfile="lexer.c" header-file="lexer.h"
%option noyywrap
%option nounput

HEX_NUM		0[Xx][0-9A-Fa-f]+
INTEGER_NUM	[0-9]+
COMMENT		";"[^\r\n]*
WS		[ \t]+
NLINE		[\r\n]
SYMBOL		[$.0-9A-Za-z_+-\\*/]+
STRING		\"([^\\\"]|\\.)*\"

%%

{WS}		; /* skip whitspace */
{COMMENT}	; /* skip comments */
{NLINE}	        line++;
{HEX_NUM}		{
			sscanf(yytext, "%x", &yylval.num);
			return T_HEX_NUM;
		}
{INTEGER_NUM}		{
			sscanf(yytext, "%d", &yylval.num);
			return T_INTEGER_NUM;
		}
{STRING}	{
			int i, j;

			for(i = 0; i < yyleng ; ++i) {
				if (yytext[i] == '\\' && yytext[i + 1] == '\"') {
					j = i;
					for( ; j < yyleng ; ++j )
						yytext[j] = yytext[j + 1];
					yyleng--;
					}
				}
			yylval.str = strdup(yytext + 1);
			if (yylval.str[yyleng - 2] != '"')
				yyerror("Improperly terminated string.");
			else
				yylval.str[yyleng - 2] = 0;
			return T_STRING;
		}
"("		return '(';
")"		return ')';
"."		return '.';
"'"		return '\'';
"`"		return '`';
","		return ',';
"@"		return '@';
"#"		return '#';

{SYMBOL}	{
 		       if (strlen(yytext) > MAX_SYM_LEN)
				yyerror("symbol exceding max length");
			yylval.str = xstrdup(yytext);
			return T_SYMBOL;
		}

.|\n		{
			yyerror("unrecognized character %c", *yytext);
		}

%%

int yyerror(const char* format, ...)
{
	va_list args;

    	fflush(stdout);

	fprintf(stderr, "Error: ");
	va_start(args, format);
	vfprintf(stderr, format, args);
	va_end(args);
	fprintf(stderr, " at line %d\n", line);

	fflush(stderr);

	exit(EXIT_FAILURE);
}
