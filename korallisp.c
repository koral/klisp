#include <stdio.h>
#include <unistd.h>

#include "korallisp.h"
#include "lisp.h"
#include "parser.h"
#include "compiler.h"

non_local_exit_t non_local_exit;

lisp_obj_t lisp_read(void);
lisp_obj_t lisp_eval(lisp_obj_t obj);

lisp_obj_t lisp_eval(lisp_obj_t obj)
{
	evaluator_t compiled_fun;
	jitter_t jitter;

	if (!(jitter = init_compiler()))
		return NIL; /* FIXME: handle error */

	if (setjmp(non_local_exit.jbuf))
		goto error;

	/* Compile */
	compiled_fun = compile(jitter, obj);

	release_compiler(jitter);
	/* And run */
	return compiled_fun();

error:
	return (lisp_obj_t)&non_local_exit;
}


int main(void)
{
	lisp_obj_t obj;
	int is_tty = isatty(fileno(stdin));

	if (init_lisp_syms())
	    goto error;

	/* REPL! */
	while (TRUE) {
		if (is_tty)
			printf("> ");
		obj = lisp_read();
		if (!obj.raw)
			break;
		obj = lisp_eval(obj);
		lisp_print(obj);
	}

	destroy_lisp_syms();

	return 0;

 error:
	fprintf (stderr, "Something went really wrong...");

	return 1;
}
