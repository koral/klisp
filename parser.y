%{
#include "lisp.h"
#include "obarray.h"

#define YYDEBUG                 1

#define YYMALLOC xmalloc
#define YYFREE xfree

int yylex(void);

void eval(void);

static lisp_obj_t yy_expr;

%}

/* These declare our output file names. */
%output "parser.c"
%defines "parser.h"
%error-verbose

%union {
	unsigned num;
	char *sym;
	char *str;
	lisp_obj_t lisp_obj;
}

%token '(' ')'
%token <num> T_INTEGER_NUM T_HEX_NUM
%token <sym> T_SYMBOL
%token <str> T_STRING

%type <lisp_obj> expr list list_content atom number

%%

read_done :
| expr					{ yy_expr = $1; return 0; }

expr
: atom					{ $$ = $1; }
| list					{ $$ = $1; }
| '\'' atom				{ $$ = lisp_cons(QUOTE,
							 lisp_cons($2, NIL)); }
| '\'' list				{ $$ = lisp_cons(QUOTE,
							 lisp_cons($2, NIL)); }

list
: '(' ')'				{ $$ = NIL; }
| '(' list_content ')'			{ $$ = $2; }
| '(' expr '.' expr ')'			{ $$ = lisp_cons($2, $4); }

list_content
: expr					{ $$ = lisp_cons($1, NIL); }
| expr list_content			{ $$ = lisp_cons($1, $2); }

atom
: T_SYMBOL				{ $$ = obarray_add_entry($1); }
| T_STRING				{ $$ = make_string($1); }
| number				{ $$ = $1; }

number
: T_INTEGER_NUM				{ $$ = make_fixnum($1); }
| T_HEX_NUM				{ $$ = make_fixnum($1); }

%%

lisp_obj_t lisp_read(void)
{
	yy_expr = (lisp_obj_t)NULL;

	if (yyparse())
		return (lisp_obj_t)NULL;
	else
		return yy_expr;
}
