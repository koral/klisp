#ifndef _korallisp_h
#define _korallisp_h
#include <stdint.h>
#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

#ifdef GC
#include <gc.h>
#endif

#ifndef COMPILER_VERBOSE_DEFAULT
/* 0 silent */
/* 1 dump stuffs to tmp dir */
/* 2 print assembly  */
/* 3 enable driver verbosity */
#define COMPILER_VERBOSE_DEFAULT 0
#endif

#define COMPILER_SPEED_DEFAULT 0

#define TRUE 1
#define FALSE 0

#define TO_BE_DONE assert(FALSE)

#define MAY_ALIAS __attribute__((__may_alias__))

#define likely(x)   __builtin_expect(!!(x), 1)
#define unlikely(x) __builtin_expect(!!(x), 0)

#define DALLOC(type, x) type x = xmalloc(sizeof(x))

/* Compile time check */
#define GLUE(a,b) __GLUE(a,b)
#define __GLUE(a,b) a ## b
#define CVERIFY(expr, msg) typedef char GLUE (compiler_verify_, msg) [(expr) ? (+1) : (-1)]

#define COMPILE_ASSERT(exp) CVERIFY (exp, __LINE__)

typedef char bool;

extern unsigned line;
int yyerror(const char* format, ...);

static inline void *xmalloc(size_t size)
{
	void *p;
#ifdef GC
	p = GC_MALLOC(size);
#else
	p = malloc(size);
#endif
	if (unlikely(!p)) {
		fprintf(stderr, "Out of memory?");
		exit (EXIT_FAILURE);
	}

	return p;
}

static inline void *xcalloc(size_t nmemb, size_t size)
{
	void *p;
	size_t real_size = nmemb * size;

#ifdef GC
	p = GC_MALLOC(real_size);
#else
	p = malloc(real_size);
#endif
	memset(p, 0, real_size);

	if (unlikely(!p)) {
		fprintf(stderr, "Out of memory?");
		exit (EXIT_FAILURE);
	}

	return p;
}

static inline void *xrealloc(void *p, size_t size)
{
#ifdef GC
	p = GC_REALLOC(p, size);
#else
	p = realloc(p, size);
#endif
	if (unlikely(!p)) {
		fprintf(stderr, "Out of memory");
		exit (EXIT_FAILURE);
	}

	return p;
}


static inline void xfree(void *p)
{
#ifdef GC
	GC_FREE(p);
#else
	free(p);
#endif
}

#define MAX_SYM_LEN 256
static inline char *xstrdup(const char *in)
{
	size_t len = strnlen(in, MAX_SYM_LEN);
	char* buf = xmalloc(len + 1);
	strncpy(buf, in, len);
	buf[len] = '\0';

	return buf;
}

#define MAX_STR_TMP_LEN 128
static inline char *
kvsnprintf(const char *format, va_list ap)
{
	char *p = xmalloc(MAX_STR_TMP_LEN);
	vsnprintf(p, MAX_STR_TMP_LEN, format, ap);

	return p;
}

static inline char *ksnprintf(const char *format, ...)
{
	char *p;
	va_list args;

	va_start(args, format);

	p = kvsnprintf(format, args);

	va_end(args);

	return p;
}

#endif
