#include <string.h>
#include "hash.h"
#include "korallisp.h"

#define HASH_INIT_SIZE 2

struct h_entry_s {
	void *payload;
	struct h_entry_s *n;
};

struct hash_table_t {
	unsigned entries;
	unsigned bucket_size;
	unsigned (*hash_f)(void *);
	int (*cmp_f)(void *, void *);
	void (*free_f)(void *);
	struct h_entry_s **h_bucket;
};

/* free the hash table content */
static void h_free(struct hash_table_t *h)
{
	unsigned i;
	struct h_entry_s *e;
	unsigned b_size = h->bucket_size;
	struct h_entry_s **h_bucket = h->h_bucket;

	for (i = 0; i < b_size; i++) {
		while ((e = h_bucket[i])) {
			h_bucket[i] = e->n;
			if (h->free_f)
				h->free_f(e->payload);
			xfree(e);
		}
	}
}

/* give a node return a pointer to and equivalent one if already exists int the
   bucket or NULL otherwise */
static void *h_search_int(struct hash_table_t *h, void *payload,
			  unsigned i_b)
{
	struct h_entry_s *e;

	e =  h->h_bucket[i_b];

	while (e) {
		if (!h->cmp_f(e->payload, payload))
			return e->payload;
		e = e->n;
	}

	return NULL;
}

static void h_grow(struct hash_table_t *h)
{
	unsigned i;
	unsigned hash_val;
	struct h_entry_s *e, *old_next;
	struct h_entry_s **old_bucket;

	old_bucket = h->h_bucket;
	h->h_bucket = xcalloc(2, h->bucket_size * sizeof(*h->h_bucket));
	h->bucket_size <<= 1;

	for (i = 0; i < h->bucket_size / 2; i++) {
		e = old_bucket[i];
		while (e) {
			old_next = e->n;
			e->n = NULL;
			hash_val = h->hash_f(e->payload) % h->bucket_size;
			if (h->h_bucket[hash_val])
				/* collision */
				e->n = h->h_bucket[hash_val];
			h->h_bucket[hash_val] = e;
			e = old_next;
		}
	}
	xfree(old_bucket);
}

void *h_add(void *h, void *payload)
{
	struct h_entry_s *e;
	struct hash_table_t *h_t = h;
	struct h_entry_s **h_bucket;
	unsigned i_b;
add:
	h_bucket = h_t->h_bucket;
	i_b = h_t->hash_f(payload) % h_t->bucket_size;

	h_remove(h, payload);

	if (h_t->entries > 2 * h_t->bucket_size) {
		h_grow(h_t);
		goto add;
	}

	e = xcalloc(1, sizeof(*e));
	e->payload = payload;

	if (h_bucket[i_b])
		/* collision */
		e->n = h_bucket[i_b];

	h_bucket[i_b] = e;
	h_t->entries++;

	return payload;
}

void h_dump(void *h)
{
	unsigned i;
	struct h_entry_s *e;
	unsigned b_size = ((struct hash_table_t *)h)->bucket_size;
	struct h_entry_s **h_bucket = ((struct hash_table_t *)h)->h_bucket;

	printf("hash table %p content:\n", h);
	for (i = 0; i < b_size; i++) {
		printf("%d: ", i);
		e = h_bucket[i];
		while (e) {
			printf("%p ", e->payload);
			e = e->n;
		}
		putchar('\n');
	}
}

void h_remove(void *h, void *payload)
{
	struct hash_table_t *h_t = h;
	struct h_entry_s **h_bucket = h_t->h_bucket;
	unsigned i_b = h_t->hash_f(payload) % h_t->bucket_size;
	struct h_entry_s *prev = NULL;
	struct h_entry_s *e;

	e = h_bucket[i_b];

	while (e) {
		if (!h_t->cmp_f(e->payload, payload))
			break;
		prev = e;
		e = e->n;
	}

	if (e) {
		if (prev)
			prev->n = e->n;
		if (h_bucket[i_b] == e)
			h_bucket[i_b] = e->n;
		xfree(e);
		h_t->entries--;
	}
}

void *hash_t_create(unsigned (*hash_f)(void *), int (*cmp_f)(void *, void *),
		    void (*free_f)(void *))
{
	struct hash_table_t *h = xcalloc(1, sizeof(*h));
	h->h_bucket = xcalloc(1, HASH_INIT_SIZE * sizeof(*h->h_bucket));
	h->bucket_size = HASH_INIT_SIZE;
	h->hash_f = hash_f;
	h->cmp_f = cmp_f;
	h->free_f = free_f;

	return (void *)h;
}

void *h_search(hash_tab_t p, void *payload)
{
	struct hash_table_t *h = p;
	unsigned i_b = h->hash_f(payload) % h->bucket_size;

	return h_search_int(h, payload, i_b);
}

void h_walk(hash_tab_t h, void (*f)(void *p, void *node), void *node)
{
	unsigned i;
	struct h_entry_s *e;
	unsigned b_size = ((struct hash_table_t *)h)->bucket_size;
	struct h_entry_s **h_bucket = ((struct hash_table_t *)h)->h_bucket;

	for (i = 0; i < b_size; i++) {
		e = h_bucket[i];
		while (e) {
			f(e->payload, node);
			e = e->n;
		}
	}
}

void hash_t_destroy(void *p)
{
	struct hash_table_t *h = p;

	h_free(h);
	xfree(h->h_bucket);
	xfree(h);
}
