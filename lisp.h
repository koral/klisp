#ifndef _lisp_h
#define _lisp_h

#include <stdint.h>
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>

#include "korallisp.h"

typedef enum {
	      CONS_T,
	      SYMBOL_T,
	      FIXNUM_T,
	      STRING_T,
	      LAMBDA_T,
	      ERROR_T,
	      GO_T
} lisp_obj_type_t;

typedef union lisp_obj_u {
	lisp_obj_type_t *_type;
	struct lisp_sym_s *_sym;
	struct lisp_fixnum_s *_num;
	struct lisp_string_s *_str;
	struct lisp_cons_s *_cons;
	struct lisp_lambda_s *_lambda;
	struct non_local_exit_s *_exit;
	void *raw;
} lisp_obj_t;

typedef struct lisp_sym_s {
	lisp_obj_type_t _type; /* Must be first */
	char *_name;
	union lisp_obj_u _val;
} lisp_sym_t;
COMPILE_ASSERT(!offsetof(lisp_sym_t, _type));

typedef struct lisp_fixnum_s {
	lisp_obj_type_t _type; /* Must be first */
	unsigned _val;
} lisp_fixnum_t;
COMPILE_ASSERT(!offsetof(lisp_fixnum_t, _type));

typedef struct lisp_string_s {
	lisp_obj_type_t _type; /* Must be first */
	char *_val;
} lisp_string_t;
COMPILE_ASSERT(!offsetof(lisp_string_t, _type));

typedef struct lisp_cons_s {
	lisp_obj_type_t _type; /* Must be first */
	union lisp_obj_u _car;
	union lisp_obj_u _cdr;
} lisp_cons_t;
COMPILE_ASSERT(!offsetof(lisp_cons_t, _type));

/* This is in use for stack unwind */
typedef struct non_local_exit_s {
	lisp_obj_type_t _type;  /* Must be first */
	jmp_buf jbuf;
	char *_val;
} non_local_exit_t;
COMPILE_ASSERT(!offsetof(non_local_exit_t, _type));

typedef struct lisp_lambda_s {
	lisp_obj_type_t _type; /* Must be first */
	void *_fun; /* FIXME this type? */
	char _arity;
} lisp_lambda_t;
COMPILE_ASSERT(!offsetof(lisp_lambda_t, _type));


extern non_local_exit_t non_local_exit;

static inline lisp_obj_type_t kl_type(lisp_obj_t obj)
{
	return *obj._type;
}

static inline bool kl_eq(lisp_obj_t a, lisp_obj_t b)
{
	return a.raw == b.raw;
}

static inline bool kl_symbolp(lisp_obj_t obj) {
	return (bool) *obj._type == SYMBOL_T;
}
static inline char *kl_symbol_name(lisp_obj_t obj) {
	assert(kl_symbolp(obj));
	return obj._sym->_name;
}
static inline lisp_obj_t kl_symbol_value(lisp_obj_t obj) {
	assert(kl_symbolp(obj));
	return obj._sym->_val;
}

static inline bool kl_fixnump(lisp_obj_t obj) {
	return (bool) obj._num->_type == FIXNUM_T;
}
static inline unsigned kl_fixnum_value(lisp_obj_t obj) {
	assert(kl_fixnump(obj));
	return obj._num->_val;
}

static inline bool kl_stringp(lisp_obj_t obj) {
	return (bool) obj._str->_type == STRING_T;
}
static inline char *kl_string_value(lisp_obj_t obj) {
	assert(kl_stringp(obj));
	return obj._str->_val;
}

static inline bool kl_gop(lisp_obj_t obj) {
	return (bool) obj._str->_type == GO_T;
}
static inline char *kl_go_value(lisp_obj_t obj) {
	assert(kl_gop(obj));
	return obj._str->_val;
}

static inline bool kl_consp(lisp_obj_t obj) {
	return (bool) obj._cons->_type == CONS_T;
}
static inline lisp_obj_t kl_car(lisp_obj_t obj) {
	assert(kl_consp(obj));
	return obj._cons->_car;
}
static inline lisp_obj_t kl_cdr(lisp_obj_t obj) {
	assert(kl_consp(obj));
	return obj._cons->_cdr;
}

int init_lisp_syms(void);
void destroy_lisp_syms(void);
lisp_obj_t make_fixnum(unsigned num);
lisp_obj_t make_string(char *str);
void throw_error(const char *format, ...);
void throw_go(const char *format, ...);
lisp_obj_t make_cons_ptr(void *ptr);

struct precooked_syms_s {
#define SYM(up_name, low_name)			\
	lisp_obj_t low_name;
#define SYM_SPECIAL(up_name, low_name, _)		\
	lisp_obj_t low_name;
#include "symbols.h"
#undef SYM
#undef SYM_SPECIAL

} precooked_syms;

#include "symbols_def.h"

lisp_obj_t lisp_atom(lisp_obj_t obj);
lisp_obj_t lisp_car(lisp_obj_t obj);
lisp_obj_t lisp_cdr(lisp_obj_t obj);
lisp_obj_t lisp_cons(lisp_obj_t car, lisp_obj_t cdr);
lisp_obj_t lisp_eq(lisp_obj_t a, lisp_obj_t b);
lisp_obj_t lisp_intern(lisp_obj_t obj);
lisp_obj_t lisp_print(lisp_obj_t obj);
lisp_obj_t lisp_setq(lisp_obj_t obj, lisp_obj_t val);

#endif
