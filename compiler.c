#include <libgccjit.h>
#include <stdlib.h>

#include "korallisp.h"
#include "compiler.h"
#include "lisp.h"
#include "block.h"
#include "kvect.h"

#ifndef LIBGCCJIT_HAVE_gcc_jit_context_set_bool_allow_unreachable_blocks
#error gcc_jit_context_set_bool_allow_unreachable_blocks missing
#endif

typedef struct {
	lisp_lambda_t *lambda;
	const char *name;
} lambda_fixup_t;
DEF_VECT(lambda_fixup_t);

typedef struct {
	gcc_jit_context *ctxt;
	gcc_jit_function *func; /* Current function being compiled */
	tag_block_t *block; /* Current basic block */
	tag_block_env_t tag_block_env; /* List of blocks for the current tagbody */
	lambda_fixup_t_vect_t *lambda_fixup_vect;
	unsigned ssa_n; /* Current temporary variable num generated */
	unsigned lambda_n; /* Current lamnda num being compiled */
	unsigned if_n; /* Current if num */
	/* Types */
	gcc_jit_type *void_ptr_t;
	gcc_jit_type *char_t;
	gcc_jit_type *char_ptr_t;
	gcc_jit_type *int_t;
	gcc_jit_type *unsigned_t;
	gcc_jit_type *lisp_obj_t;
	/* External functions we can call */
	gcc_jit_function *atom_f;
	gcc_jit_function *eq_f;
	gcc_jit_function *car_f;
	gcc_jit_function *cdr_f;
	gcc_jit_function *cons_f;
	gcc_jit_function *set_f;
	gcc_jit_function *print_f;
	gcc_jit_function *symbol_value_f;
	/* Some const value doesn't change */
	gcc_jit_rvalue *nil;
} j_t;

static gcc_jit_lvalue *compile_rec(j_t *j, lisp_obj_t obj);

static gcc_jit_lvalue *gen_ssa_var(j_t *j)
{
	char *p = ksnprintf("x%d", j->ssa_n++);

	return gcc_jit_function_new_local(j->func,
					NULL,
					j->lisp_obj_t,
					p);
}

/* compile lisp.h types */
static void compile_lisp_types(j_t *j)
{
	j->int_t =
		gcc_jit_context_get_type(j->ctxt, GCC_JIT_TYPE_INT);
	j->unsigned_t =
		gcc_jit_context_get_type(j->ctxt, GCC_JIT_TYPE_UNSIGNED_INT);
	j->char_t =
		gcc_jit_context_get_type(j->ctxt, GCC_JIT_TYPE_CHAR);
	j->char_ptr_t =
		gcc_jit_type_get_pointer(j->char_t);
	j->void_ptr_t =
		gcc_jit_context_get_type(j->ctxt, GCC_JIT_TYPE_VOID_PTR);
	j->lisp_obj_t =
		gcc_jit_context_get_type(j->ctxt, GCC_JIT_TYPE_VOID_PTR);
}

/* declare into the jitter all the helper funcs we need */
static void compile_helpers(j_t *j)
{
	/* lisp_atom */
	gcc_jit_param *atom_param =
		gcc_jit_context_new_param(
				j->ctxt,
				NULL,
				j->lisp_obj_t,
				"a");
	j->atom_f =
		gcc_jit_context_new_function(j->ctxt, NULL,
					GCC_JIT_FUNCTION_IMPORTED,
					j->lisp_obj_t,
					"lisp_atom",
					1, &atom_param,
					1);

	/* lisp_eq */
	gcc_jit_param *eq_param[2] =
		{ gcc_jit_context_new_param(
				j->ctxt,
				NULL,
				j->lisp_obj_t,
				"a") ,
		  gcc_jit_context_new_param(
			  j->ctxt,
			  NULL,
			  j->lisp_obj_t,
			  "b") };

	j->eq_f =
		gcc_jit_context_new_function(j->ctxt, NULL,
					GCC_JIT_FUNCTION_IMPORTED,
					j->lisp_obj_t,
					"lisp_eq",
					2, eq_param,
					1);

	/* lisp_car */
	gcc_jit_param *car_param =
		gcc_jit_context_new_param(
				j->ctxt,
				NULL,
				j->lisp_obj_t,
				"a");
	j->car_f =
		gcc_jit_context_new_function(j->ctxt, NULL,
					GCC_JIT_FUNCTION_IMPORTED,
					j->lisp_obj_t,
					"lisp_car",
					1, &car_param,
					1);
	/* lisp_cdr */
	gcc_jit_param *cdr_param =
		gcc_jit_context_new_param(
				j->ctxt,
				NULL,
				j->lisp_obj_t,
				"a");
	j->cdr_f =
		gcc_jit_context_new_function(j->ctxt, NULL,
					GCC_JIT_FUNCTION_IMPORTED,
					j->lisp_obj_t,
					"lisp_cdr",
					1, &cdr_param,
					1);
	/* lisp_set */
	gcc_jit_param *set_param =
	  gcc_jit_context_new_param(
				    j->ctxt,
				    NULL,
				    j->lisp_obj_t,
				    "a");

	j->set_f =
		gcc_jit_context_new_function(j->ctxt, NULL,
					GCC_JIT_FUNCTION_IMPORTED,
					j->lisp_obj_t,
					"lisp_setq",
					1, &set_param,
					1);

	/* lisp_cons */
	gcc_jit_param *cons_param[2] =
		{ gcc_jit_context_new_param(
				j->ctxt,
				NULL,
				j->lisp_obj_t,
				"a") ,
		  gcc_jit_context_new_param(
			  j->ctxt,
			  NULL,
			  j->lisp_obj_t,
			  "b") };

	j->cons_f =
		gcc_jit_context_new_function(j->ctxt, NULL,
					GCC_JIT_FUNCTION_IMPORTED,
					j->lisp_obj_t,
					"lisp_cons",
					2, cons_param,
					1);
	/* lisp_print */
	gcc_jit_param *print_param =
		gcc_jit_context_new_param(
				j->ctxt,
				NULL,
				j->lisp_obj_t,
				"a");
	j->print_f =
		gcc_jit_context_new_function(j->ctxt, NULL,
					GCC_JIT_FUNCTION_IMPORTED,
					j->lisp_obj_t,
					"lisp_print",
					1, &print_param,
					1);
	/* lisp_symbol_value */
	gcc_jit_param *symbol_value_param =
		gcc_jit_context_new_param(
				j->ctxt,
				NULL,
				j->lisp_obj_t,
				"a");
	j->symbol_value_f =
		gcc_jit_context_new_function(j->ctxt, NULL,
					GCC_JIT_FUNCTION_IMPORTED,
					j->lisp_obj_t,
					"lisp_symbol_value",
					1, &symbol_value_param,
					1);

	j->nil =
		gcc_jit_context_new_rvalue_from_ptr(j->ctxt,
						j->lisp_obj_t,
						NIL.raw);
}

/* evaluate a symbol */
static gcc_jit_lvalue *compile_symbol(j_t *j, lisp_obj_t obj)
{
	assert(kl_symbolp(obj));

	/*
	  xi = 0x...;
	*/

	gcc_jit_lvalue *xi = gen_ssa_var(j);
	lisp_obj_t sym_val;

	sym_val.raw = kl_symbol_value(obj).raw;

	if (kl_fixnum_value(kl_symbol_value(KL_SPEED)) >= 3 ||
		!sym_val.raw) {
		if (!sym_val.raw) {
			printf("Warning: compiling reference to unbound symbol \"%s\". "
				"Assuming it global.\n", kl_symbol_name(obj));
		/*
		Assuming the user is doing something harmful we compile the
		symbol access using as safe wrapper 'lisp_symbol_value'.
		Note: in case the symbol is now bound but will be unbound
		later on we'll not be able to give the symbol name in the error
		at run-time.
		*/
		gcc_jit_rvalue *param = gcc_jit_context_new_rvalue_from_ptr(
			j->ctxt,
			j->lisp_obj_t,
			obj.raw);

		gcc_jit_block_add_assignment(
			j->block->gcc_block, NULL,
			xi,
			gcc_jit_context_new_call(
				j->ctxt,
				NULL,
				j->symbol_value_f,
				1, &param));
		}
	} else {
		gcc_jit_block_add_assignment(
			j->block->gcc_block , NULL,
			xi,
			gcc_jit_context_new_rvalue_from_ptr(
				j->ctxt,
				j->lisp_obj_t,
				sym_val.raw));
	}

	return xi;
}

/* this is for all atoms that gets evaluate to them self */
static gcc_jit_lvalue *compile_self_evaluating(j_t *j, lisp_obj_t obj)
{
	assert(!kl_symbolp(obj));
	assert(!kl_consp(obj));

	/*
	  xi = 0x...;
	*/

	gcc_jit_lvalue *xi = gen_ssa_var(j);

	gcc_jit_block_add_assignment(
		j->block->gcc_block, NULL,
		xi,
		gcc_jit_context_new_rvalue_from_ptr(
			j->ctxt,
			j->lisp_obj_t,
			obj.raw));

	return xi;
}

/* Compiler for all special forms */

static void compile_atom(j_t *j, lisp_obj_t cdr, gcc_jit_lvalue *xi)
{
	/* xi = atom (`xi-1`); */

	gcc_jit_rvalue *xi_min1 =
		gcc_jit_lvalue_as_rvalue(compile_rec(j, kl_car(cdr)));

	gcc_jit_block_add_assignment(
		j->block->gcc_block, NULL,
		xi,
		gcc_jit_context_new_call(
			j->ctxt,
			NULL,
			j->atom_f,
			1, &xi_min1));
}

static void compile_car(j_t *j, lisp_obj_t cdr, gcc_jit_lvalue *xi)
{
	/* xi = car (`xi-1`); */

	gcc_jit_rvalue *xi_min1 =
		gcc_jit_lvalue_as_rvalue(compile_rec(j, kl_car(cdr)));

	gcc_jit_block_add_assignment(
		j->block->gcc_block, NULL,
		xi,
		gcc_jit_context_new_call(
			j->ctxt,
			NULL,
			j->car_f,
			1, &xi_min1));
}

static void compile_cdr(j_t *j, lisp_obj_t cdr, gcc_jit_lvalue *xi)
{
	/* xi = cdr (`xi-1`); */

	gcc_jit_rvalue *xi_min1 =
		gcc_jit_lvalue_as_rvalue(compile_rec(j, kl_car(cdr)));

	gcc_jit_block_add_assignment(
		j->block->gcc_block, NULL,
		xi,
		gcc_jit_context_new_call(
			j->ctxt,
			NULL,
			j->cdr_f,
			1, &xi_min1));

}

static void compile_cons(j_t *j, lisp_obj_t cdr, gcc_jit_lvalue *xi)
{
	/* xi = cons (xa, xb); */

	gcc_jit_rvalue *cons_args[2] =
		{ gcc_jit_lvalue_as_rvalue(compile_rec(j, kl_car(cdr))) ,
		  gcc_jit_lvalue_as_rvalue(
			  compile_rec(j, kl_car(kl_cdr(cdr)))) };

	gcc_jit_block_add_assignment(
		j->block->gcc_block, NULL,
		xi,
		gcc_jit_context_new_call(j->ctxt,
					NULL,
					j->cons_f,
					2,
					cons_args));
}

static void compile_eq(j_t *j, lisp_obj_t cdr, gcc_jit_lvalue *xi)
{
	/* xi = lisp_eq (`xi-2` `xi-1`); */

	gcc_jit_rvalue *eq_args[2] =
		{ gcc_jit_lvalue_as_rvalue(compile_rec(j, kl_car(cdr))),
		  gcc_jit_lvalue_as_rvalue(
			  compile_rec(j, kl_car(kl_cdr(cdr)))) };

	gcc_jit_block_add_assignment(
		j->block->gcc_block, NULL,
		xi,
		gcc_jit_context_new_call(
			j->ctxt,
			NULL,
			j->eq_f,
			2, eq_args));

	}

static void compile_if(j_t *j, lisp_obj_t cdr, gcc_jit_lvalue *xi)
{
	/* if (`xi-1` != NIL)
	   xi = ... a
	   else
	   xi = ... b */

	tag_block_t *orig_block = j->block;

	char *reun_name = ksnprintf("reunification-%d", j->if_n);
	char *then_name = ksnprintf("then-%d", j->if_n);
	char *else_name = ksnprintf("else-%d", j->if_n);
	j->if_n++;

	/* This is the condition */
	gcc_jit_rvalue *xi_min1 =
		gcc_jit_lvalue_as_rvalue(compile_rec(j, kl_car(cdr)));

	gcc_jit_rvalue *test =
		gcc_jit_context_new_comparison(
			j->ctxt,
			NULL,
			GCC_JIT_COMPARISON_NE,
			xi_min1, j->nil);


	tag_block_t *then_block =
		tag_block_create(j->tag_block_env, j->func, then_name);

	j->block = then_block;

	gcc_jit_rvalue *val_a =
		gcc_jit_lvalue_as_rvalue(
			compile_rec(j, kl_car(kl_cdr(cdr))));
	tag_block_t *after_then_block = j->block;
	if (!after_then_block->terminated)
		gcc_jit_block_add_assignment(
			after_then_block->gcc_block, NULL,
			xi, val_a);

	tag_block_t *else_block =
		tag_block_create(j->tag_block_env, j->func, else_name);

	j->block = else_block;

	gcc_jit_rvalue *val_b =
		gcc_jit_lvalue_as_rvalue(
			compile_rec(j, kl_car(kl_cdr(kl_cdr(cdr)))));
	tag_block_t *after_else_block = j->block;
	if (!after_else_block->terminated)
		gcc_jit_block_add_assignment(
			after_else_block->gcc_block, NULL,
			xi, val_b);

	tag_block_end_with_conditional(orig_block,
				NULL,
				test,
				then_block,
				else_block);

	tag_block_t *reunification_block =
		tag_block_create(j->tag_block_env, j->func, reun_name);

	if (!after_then_block->terminated)
		tag_block_end_with_jump(
			after_then_block, NULL, reunification_block);
	if (!after_else_block->terminated)
		tag_block_end_with_jump(
			after_else_block, NULL, reunification_block);

	j->block = reunification_block;

}

static void compile_quote(j_t *j, lisp_obj_t cdr, gcc_jit_lvalue *xi)
{
	/* xi = 0x...; */

	gcc_jit_block_add_assignment(
		j->block->gcc_block, NULL,
		xi,
		gcc_jit_context_new_rvalue_from_ptr(
			j->ctxt,
			j->lisp_obj_t,
			kl_car(cdr).raw));

}
static void compile_setq(j_t *j, lisp_obj_t cdr, gcc_jit_lvalue *xi)
{
	/* xi = set(sym `xi-1`); */

	lisp_obj_t cadr = kl_car(cdr); /* Symbol to bind */
	lisp_obj_t caddr = kl_car(kl_cdr(cdr)); /* rval to bind */

	gcc_jit_rvalue *sym = gcc_jit_context_new_rvalue_from_ptr(
			j->ctxt,
			j->lisp_obj_t,
			cadr.raw);

	gcc_jit_rvalue *xi_min1 =
		gcc_jit_lvalue_as_rvalue(compile_rec(j, caddr));

	gcc_jit_rvalue *setq_args[2] = { sym, xi_min1 };

	gcc_jit_block_add_assignment(
		j->block->gcc_block, NULL,
		xi,
		gcc_jit_context_new_call(
			j->ctxt,
			NULL,
			j->set_f,
			2, setq_args));
}

static void compile_tagbody(j_t *j, lisp_obj_t cdr, gcc_jit_lvalue *xi)
{
	lisp_obj_t el;
	tag_block_env_t prev_tag_block_env = j->tag_block_env;
	lisp_obj_t p = cdr;
	int i = 0;

	j->tag_block_env = tag_block_env_init();

	/* Collect and hash all basic blocks */
	while (!kl_eq(p, NIL)) {
		el = kl_car(p);
		if (kl_symbolp(el)) {
			tag_block_create(j->tag_block_env, j->func, kl_symbol_name(el));
		}
		p = kl_cdr(p);
		i++;
	}

	p = cdr;

	while (!kl_eq(p, NIL)) {
		el = kl_car(p);
		if (kl_symbolp(el)) {
			/* In case a symbol is encourred a new active basic
			   block is set */
			tag_block_t *tb =
				tag_block_search(j->tag_block_env,
						kl_symbol_name(el));
			assert(tb);
			if (!j->block->terminated)
				tag_block_end_with_jump(
					j->block, NULL, tb);
			j->block = tb;
		} else {
			compile_rec(j, el);
		}
		p = kl_cdr(p);
	}

	/* Always return nil */
	if (!j->block->terminated)
		gcc_jit_block_add_assignment(
			j->block->gcc_block, NULL,
			xi,
			gcc_jit_context_new_rvalue_from_ptr(
				j->ctxt,
				j->lisp_obj_t,
				NIL.raw));

	tag_block_env_destroy(j->tag_block_env);
	j->tag_block_env = prev_tag_block_env;
}

static void compile_go(j_t *j, lisp_obj_t cdr, gcc_jit_lvalue *xi)
{
	lisp_obj_t tag = kl_car(cdr);

	assert(kl_symbolp(tag));
	char *tag_name  = kl_symbol_name(tag);

	tag_block_t *tb = tag_block_search(j->tag_block_env, tag_name);
	if (tb) {
		/* This a local go */
		tag_block_end_with_jump(j->block, NULL, tb);
	} else {
		/* Lets unwind the stack */
		throw_go(tag_name);
	}
}

static void compile_print(j_t *j, lisp_obj_t cdr, gcc_jit_lvalue *xi)
{
	/* xi = print (`xi-1`); */

 	gcc_jit_rvalue *xi_min1 =
		gcc_jit_lvalue_as_rvalue(compile_rec(j, kl_car(cdr)));

	gcc_jit_block_add_assignment(
		j->block->gcc_block, NULL,
		xi,
		gcc_jit_context_new_call(
			j->ctxt,
			NULL,
			j->print_f,
			1, &xi_min1));
}

static void compile_lambda(j_t *j, lisp_obj_t l_list_body, gcc_jit_lvalue *xi)
{
	gcc_jit_function *old_fn = j->func;
	tag_block_t *old_block = j->block;
	tag_block_env_t old_tag_block_env = j->tag_block_env;

	j->tag_block_env = tag_block_env_init();

	char *name = ksnprintf("lambda_%d", j->lambda_n++);

	j->func = gcc_jit_context_new_function(j->ctxt, NULL,
						GCC_JIT_FUNCTION_EXPORTED,
						j->lisp_obj_t,
						name,
						0, NULL,
						0);
	j->block = tag_block_create(j->tag_block_env, j->func, "main_block");

	/* lisp_obj_t lambda_list = kl_car(l_list_body); */
	lisp_obj_t body = kl_cdr(l_list_body);

	/* FIXME add implicit progn */
	gcc_jit_lvalue *xn = compile_rec(j, kl_car(body));

	if (!j->block->terminated)
		tag_block_end_with_return(j->block, NULL,
					gcc_jit_lvalue_as_rvalue(xn));

	tag_block_env_destroy(j->tag_block_env);
	j->tag_block_env = old_tag_block_env;
	j->block = old_block;
	j->func = old_fn;

	DALLOC(lisp_lambda_t, *lambda_obj);
	lambda_obj->_type = LAMBDA_T;
	lambda_obj->_fun = NULL;
	lambda_obj->_arity = 0; /* FIXME */

	lambda_fixup_t lambda_fixup;
	lambda_fixup.lambda = lambda_obj;
	lambda_fixup.name = name;
	append_lambda_fixup_t(&j->lambda_fixup_vect, lambda_fixup);

	gcc_jit_block_add_assignment(
		j->block->gcc_block, NULL,
		xi,
		gcc_jit_context_new_rvalue_from_ptr(
			j->ctxt,
			j->lisp_obj_t,
			lambda_obj));
}

static gcc_jit_lvalue *compile_list(j_t *j, lisp_obj_t obj)
{
	lisp_obj_t first = kl_car(obj);
	lisp_obj_t rest = kl_cdr(obj);

	assert(kl_consp(obj));
	assert(kl_symbolp(first));

	gcc_jit_lvalue *xi = gen_ssa_var(j);

	if (kl_eq(first, ATOM))
		compile_atom(j, rest, xi);
	else if (kl_eq(first, CAR))
		compile_car(j, rest, xi);
	else if (kl_eq(first, CDR))
		compile_cdr(j, rest, xi);
	else if (kl_eq(first, CONS))
		compile_cons(j, rest, xi);
	else if (kl_eq(first, EQ))
		compile_eq(j, rest, xi);
	else if (kl_eq(first, IF))
		compile_if(j, rest, xi);
	else if (kl_eq(first, PRINT))
		compile_print(j, rest, xi);
	else if (kl_eq(first, QUOTE))
		compile_quote(j, rest, xi);
	else if (kl_eq(first, SETQ))
		compile_setq(j, rest, xi);
	else if (kl_eq(first, TAGBODY))
		compile_tagbody(j, rest, xi);
	else if (kl_eq(first, GO))
		compile_go(j, rest, xi);
	else if (kl_eq(first, LAMBDA))
		compile_lambda(j, rest, xi);
	else
		throw_error("unknown form \"%s\"", kl_symbol_name(first));

	return xi;
}

static gcc_jit_lvalue *compile_rec(j_t *j, lisp_obj_t obj)
{
	if (!obj.raw ||
	    j->block->terminated)
		return NULL;

	switch (kl_type(obj)) {
		case CONS_T:
			return compile_list(j, obj);
		case SYMBOL_T:
			return compile_symbol(j, obj);
		default:
			return compile_self_evaluating(j, obj);
	}

	return NULL;
}

evaluator_t compile(jitter_t jitter, lisp_obj_t obj)
{
	evaluator_t fn;
	gcc_jit_result *result;
	j_t *j = (j_t *)jitter;

	assert(j->ctxt);

	/*
	lisp_obj_t eval_fn(void) {

		...

		return xn;
	}
	*/

	j->func = gcc_jit_context_new_function(j->ctxt, NULL,
						GCC_JIT_FUNCTION_EXPORTED,
						j->lisp_obj_t,
						"eval_fn",
						0, NULL,
						0);

	j->block = tag_block_create(j->tag_block_env, j->func, "main_block");

	gcc_jit_lvalue *xn = compile_rec(j, obj);

	if (!j->block->terminated)
		tag_block_end_with_return(j->block, NULL,
					gcc_jit_lvalue_as_rvalue(xn));

	/* Compile the code.  */
	result = gcc_jit_context_compile(j->ctxt);
	if (!result)
		goto error;
	fn = (evaluator_t)gcc_jit_result_get_code(result, "eval_fn");
	if (!fn)
		goto error;

	/* Once compilation is done we need to fixup all compiled lambdas */
	lambda_fixup_t lambda_fixup;
	for (unsigned i = 0; i < KV_N_ELEM(j->lambda_fixup_vect); i++) {
		lambda_fixup = KV_EL(j->lambda_fixup_vect, i);
		lambda_fixup.lambda->_fun =
			gcc_jit_result_get_code(result, lambda_fixup.name);
	}

	if (kl_fixnum_value(kl_symbol_value(KL_VERBOSE)) > 1) {
		gcc_jit_context_compile_to_file(j->ctxt,
						GCC_JIT_OUTPUT_KIND_ASSEMBLER,
						"tmp.s");
		system("cat tmp.s");
	}

	return fn;

error:
	fprintf (stderr, "Compilation failed");

	return NULL;
}

jitter_t init_compiler(void)
{
	DALLOC(j_t, *j);

	j->ssa_n = 0;
	j->lambda_n = 0;

	j->tag_block_env = tag_block_env_init();

	j->lambda_fixup_vect = init_lambda_fixup_t_vect();

	/* Get a "context" object for working with the library.  */
	j->ctxt = gcc_jit_context_acquire();
	if (!j->ctxt)
		return NULL;

	gcc_jit_context_set_bool_allow_unreachable_blocks(j->ctxt, 1);

	/* Set some options on the context.
	   Let's see the code being generated, in assembler form.  */

	if (kl_fixnum_value(kl_symbol_value(KL_VERBOSE))) {
		gcc_jit_context_set_bool_option(
			j->ctxt,
			GCC_JIT_BOOL_OPTION_DUMP_EVERYTHING,
			1);

		gcc_jit_context_set_bool_option(
			j->ctxt,
			GCC_JIT_BOOL_OPTION_KEEP_INTERMEDIATES,
			1);
	}

	if (kl_fixnum_value(kl_symbol_value(KL_VERBOSE)) > 2)
		gcc_jit_context_add_driver_option(j->ctxt, "-v");

	compile_lisp_types(j);

	compile_helpers(j);

	return (jitter_t)j;
}

void release_compiler(jitter_t jitter)
{
	j_t *j = (j_t *)jitter;

	tag_block_env_destroy(j->tag_block_env);
	gcc_jit_context_release(j->ctxt);
}
