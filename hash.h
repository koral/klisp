#ifndef __HASH_H__
#define __HASH_H__

typedef void* hash_tab_t;

/* create and return an hash table */
hash_tab_t hash_t_create(unsigned (*hash_f)(void *),
			 int (*cmp_f)(void *, void *),
			 void (*free_f)(void *));

/* free an hash table and its content (if free_f was specified) */
void hash_t_destroy(hash_tab_t h);

/* add an entry to the hash table, return the pointer to the entry.
   supersede old entry in case */
void *h_add(hash_tab_t h, void *payload);

/* give a node return a pointer to and equivalent one if already exists int the
   bucket or NULL otherwise */
void *h_search(hash_tab_t, void *payload);

/* if present remove the equivalent entry from the hash.
   in case a free function  was specified the entry is also freed. */
void h_remove(hash_tab_t h, void *payload);

/* dump to stdout hash table content */
void h_dump(hash_tab_t h);

/* walk the whole hash table */
void h_walk(hash_tab_t h, void (*f)(void *p, void *node), void *node);

#endif
