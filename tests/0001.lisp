;; some minimal test
t
nil
'a
1
"foo"
'(1 2 3)
'(1 . 2)
(cons 'x 'y)
(car '(1 2 3))
(cdr '(1 2 3))

(eq 'x 'x)
(eq 'x 'y)

(atom 'a)
(atom '(1 2 3))

(setq a 'foo)
a

(setq a nil)
a
