;; Control flow stuffs

"*** if tests ***"

(if t
    "test match"
    "test does not match")

(if nil
    "test match"
    "test does not match")

(if nil
    "wrong"
    (if t
	"correct"
	"wrong"))

(if nil
    "wrong"
    (if nil
	"wrong"
	"correct"))

(if t
    "correct"
    (if t
	"wrong"
	"wrong"))
(if t
    "correct"
    (if nil
	"wrong"
	"wrong"))

"*** tagbody tests ***"

(tagbody
 label
   (print "once")
   (if nil (go label)
       (print "exit")))

(tagbody
 label1
   (print "once")
   (go label3)
 label2
   (if t
       (go exit)
       (go label1))
   (print "never")
 label3
   (print "second")
   (go label2)
 exit
   (print "done"))

(tagbody
 label
   (print "once")
   (tagbody
      (print "second")
      (go label)
      (print "never")
    label
      (print "last")))
