#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include "kvect_base.h"
#include "korallisp.h"

#define INIT_SIZE (2 << 8)

extern struct vect_s *init_vect(size_t sizeof_elem)
{
	struct vect_s *coll = NULL;

	resize_vect(&coll, sizeof_elem);

	return coll;
}

extern void resize_vect(struct vect_s **coll_ref, const size_t sizeof_elem)
{
	size_t new_byte_size, old_byte_size, new_el_len;
	struct vect_s *coll = *coll_ref;

	assert(sizeof_elem);

	if (unlikely(!coll)) {
		new_byte_size = INIT_SIZE * sizeof_elem +
			sizeof(struct vect_s);
		old_byte_size = 0;
		new_el_len = INIT_SIZE;
	} else {
		old_byte_size = coll->max_len * sizeof_elem +
			sizeof(struct vect_s);
		new_byte_size = 2 * (coll->max_len * sizeof_elem) +
			sizeof(struct vect_s);
		new_el_len = 2 * coll->max_len;
	}

	coll = xrealloc(coll, new_byte_size);

	memset((char *)coll + old_byte_size, 0, new_byte_size - old_byte_size);
	coll->max_len = new_el_len;
	*coll_ref = coll;

	return;
}
