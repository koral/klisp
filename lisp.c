#define _GNU_SOURCE
#include <search.h>

#include "lisp.h"
#include "obarray.h"
#include "korallisp.h"

int init_lisp_syms(void)
{
	int res = obarray_init();

	if (res)
		return res;

#define SYM(up_name, low_name)			\
	up_name = obarray_add_entry(#low_name);
#define SYM_SPECIAL(up_name, low_name, real_name)	\
	up_name = obarray_add_entry(#real_name);
#include "symbols.h"
#undef SYM
#undef SYM_SPECIAL

	lisp_setq(T, T);
	lisp_setq(NIL, NIL);

	lisp_setq(KL_VERBOSE, make_fixnum(COMPILER_VERBOSE_DEFAULT));
	lisp_setq(KL_SPEED, make_fixnum(COMPILER_SPEED_DEFAULT));

	return 0;
}

void destroy_lisp_syms(void)
{
	obarray_destroy();
}

lisp_obj_t make_fixnum(unsigned num)
{
	DALLOC(lisp_fixnum_t, *obj);

	obj->_type = FIXNUM_T;
	obj->_val = num;

	return (lisp_obj_t)obj;
};

lisp_obj_t make_string(char *str)
{
	DALLOC(lisp_string_t, *obj);

	obj->_type = STRING_T;
	obj->_val = str;

	return (lisp_obj_t)obj;
};

void throw_error(const char *format, ...)
{
	va_list args;
	va_start(args, format);
	char *err_msg = kvsnprintf(format, args);
	va_end(args);

	non_local_exit._type = ERROR_T;
	non_local_exit._val = err_msg;
	longjmp(non_local_exit.jbuf , 1);
}

void throw_go(const char *format, ...)
{
	va_list args;
	va_start(args, format);
	char *label_name = kvsnprintf(format, args);
	va_end(args);

	non_local_exit._type = GO_T;
	non_local_exit._val = label_name;
	longjmp(non_local_exit.jbuf, 1);
}

char *go_unwindth_set(void)
{
	non_local_exit_t non_local_exit_tmp;

	non_local_exit_tmp = non_local_exit;

	if (!setjmp(non_local_exit.jbuf))
		return 0;

	if (non_local_exit._type != GO_T)
		/* We are unwinding the stack but is not because of a go */
		return 0;

	non_local_exit = non_local_exit_tmp;

	return non_local_exit._val;
}

lisp_obj_t lisp_atom(lisp_obj_t obj)
{
	return kl_consp(obj) ? NIL : T;
}

lisp_obj_t lisp_eq(lisp_obj_t a, lisp_obj_t b)
{
	return (kl_eq(a, b)) ? T : NIL;
}

lisp_obj_t lisp_car(lisp_obj_t obj)
{
	return kl_car(obj);
}

lisp_obj_t lisp_cdr(lisp_obj_t obj)
{
	return kl_cdr(obj);
}

lisp_obj_t lisp_cons(lisp_obj_t car, lisp_obj_t cdr)
{
	DALLOC(lisp_cons_t, *cons);

	cons->_type = CONS_T;
	cons->_car = car;
	cons->_cdr = cdr;

	return (lisp_obj_t)cons;
}

lisp_obj_t lisp_setq(lisp_obj_t obj,  lisp_obj_t val)
{
	assert(kl_symbolp(obj));

	obj._sym->_val.raw = val.raw;

	return val;
}

lisp_obj_t lisp_intern(lisp_obj_t obj)
{
	assert(kl_stringp(obj));

	return obarray_add_entry(obj._str->_val);
}

lisp_obj_t lisp_symbol_value(lisp_obj_t obj)
{
	lisp_obj_t value = kl_symbol_value(obj);

	if (!value.raw)
		throw_error("symbol \"%s\" is unbound", kl_symbol_name(obj));

	return value;
}

#define CONS_STYLE FALSE /* Print lists using basic cons notation */

static void lisp_print_internal(lisp_obj_t obj);

static void print_list(lisp_obj_t obj)
{
	if (CONS_STYLE) {
		printf("(cons ");
		lisp_print_internal(kl_car(obj));
		putchar(' ');
		lisp_print_internal(kl_cdr(obj));
		putchar(')');
	} else {
		printf("(");
		assert(kl_cdr(obj).raw);
		if (kl_consp(kl_cdr(obj)))
			do {
				lisp_print_internal(kl_car(obj));
				if (!kl_eq(kl_cdr(obj), NIL))
					putchar(' ');
				obj = kl_cdr(obj);
			} while (!kl_eq(obj, NIL));
		else {
			lisp_print_internal(obj._cons->_car);
			printf(" . ");
			lisp_print_internal(kl_cdr(obj));
		}
		putchar(')');
	}
}

static void lisp_print_internal(lisp_obj_t obj)
{
	if (!obj.raw) {
		fprintf(stderr, "Error: reference to unbound symbol?\n");
		return;
	}

	switch (kl_type(obj)) {
	case CONS_T:
		print_list(obj);
		break;
	case SYMBOL_T:
		printf("%s", obj._sym->_name);
		break;
	case FIXNUM_T:
		printf("%d", obj._num->_val);
		break;
	case STRING_T:
		printf("\"%s\"", obj._str->_val);
		break;
	case LAMBDA_T:
		printf("#<FUNCTION {%p}>", obj._lambda->_fun);
		break;
	case ERROR_T:
		fprintf(stderr, "Error: %s.\n", obj._exit->_val);
		break;
	default:
		assert(0);
	}
}

lisp_obj_t lisp_print(lisp_obj_t obj)
{
	lisp_print_internal(obj);
	putchar('\n');

	return obj;
}
