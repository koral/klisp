#ifdef SYM_DEF

#define HASH_LIT #
#define HASH() HASH_LIT
#define SYM(up_name, low_name)				\
	HASH()define up_name	precooked_syms.low_name
#define SYM_SPECIAL(up_name, low_name, _)		\
	HASH()define up_name	precooked_syms.low_name
#endif

SYM(T, t)
SYM(NIL, nil)
/* special forms */
SYM(ATOM, atom)
SYM(CAR, car)
SYM(CDR, cdr)
SYM(CONS, cons)
SYM(EQ, eq)
SYM_SPECIAL(IF, if_, if)
SYM(PRINT, print)
SYM(QUOTE, quote)
SYM(SETQ, setq)
SYM(TAGBODY, tagbody)
SYM(GO, go)
SYM(LAMBDA, lambda)
SYM_SPECIAL(KL_VERBOSE, kl_verbose, kl:verbose)
SYM_SPECIAL(KL_SPEED, kl_speed, kl:speed)
