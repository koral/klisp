#define _GNU_SOURCE
#include <assert.h>

#include "lisp.h"
#include "hash.h"

hash_tab_t obarray;

static unsigned hash_entry(void *ptr)
{
	unsigned res;
	lisp_obj_t sym;

	sym.raw = ptr;
	char *name = kl_symbol_name(sym);

	while (*name) {
		res = *name;
		name++;
	}
	return res;
}

static int cmp_entry(void *a, void *b)
{
	lisp_obj_t xa, xb;

	xa.raw = a;
	xb.raw = b;

	return strcmp(kl_symbol_name(xa), kl_symbol_name(xb));
}

int obarray_init(void)
{
	obarray = hash_t_create(hash_entry, cmp_entry, NULL);
	if (!obarray)
		return -1;

	return 0;
}

lisp_obj_t obarray_add_entry(char *sym_name)
{
	lisp_sym_t *res;
	DALLOC(lisp_sym_t, *sym);
	sym->_type = SYMBOL_T;
	sym->_name = sym_name;
	sym->_val.raw = NULL;

	if ((res = h_search(obarray, sym)))
		return (lisp_obj_t)res;

	h_add(obarray, sym);

	return (lisp_obj_t)sym;
}

void obarray_destroy(void)
{
	hash_t_destroy(obarray);
}
