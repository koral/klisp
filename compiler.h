#ifndef _compiler_h
#define _compiler_h

#include "lisp.h"

typedef lisp_obj_t (*evaluator_t) (void);
typedef void *jitter_t;

jitter_t init_compiler(void);
void release_compiler(jitter_t jitter);
evaluator_t compile(jitter_t jitter, lisp_obj_t obj);

#endif
