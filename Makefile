SRCS =  korallisp.c parser.c lexer.c compiler.c hash.c obarray.c kvect_base.c
HEADERS = korallisp.h compiler.h hash.h obarray.h block.h kvect.h kvect_base.h
HELPER_SRCS = lisp.c
HELPER_HEADERS = lisp.h
CC = gcc
CFLAGS = -O0 -g3 -Wall -I/usr/local/include/
LEX = flex
YACC = bison
YACC_OPT = #--report=all
TEST_DIR = tests

%.c: %.y
%.c: %.l

default: korallisp

.PHONY: tests
tests: korallisp
	make -C $(TEST_DIR) tests

symbols_def.h: symbols.h
	gcc -E $< -o $@ -D SYM_DEF

korallisp: $(SRCS) $(HEADERS) symbols_def.h libhelper.so
	$(CC) $(CFLAGS) $(SRCS) -o $@ -L ./ -lhelper -lgc -DGC -lgccjit -Wl,-rpath -Wl,.

libhelper.so: $(HELPER_SRCS) $(HELPER_HEADERS)
	$(CC) $(CFLAGS) -fPIC -shared $(HELPER_SRCS) -o $@

lexer.c: lexer.l
	$(LEX) lexer.l

parser.c: parser.y lexer.l
	$(YACC) $(YACC_OPT) parser.y

compilation_db:
	bear make korallisp -B

clean:
	rm -rf *.o lexer.c lexer.h parser.c parser.h korallisp
