#ifndef _kvectbase_h_included_
#define _kvectbase_h_included_

#include <stddef.h>

struct vect_s {
	size_t len;
	size_t max_len;
};

extern struct vect_s *init_vect(size_t sizeof_elem);

extern void resize_vect(struct vect_s **coll_ref, const size_t sizeof_elem);

extern void shrink_vect(struct vect_s **coll_ref, const size_t sizeof_elem);

#endif
